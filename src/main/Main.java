/**
 * 
 */
package main;

import java.io.File;

import action.Commande;
import action.Gestion;
import action.ScannerUtil;

/**
 * Classe Main : permet de lancer l'application
 * 
 * @author 59013-07-04
 *
 */
public class Main {

    /**
     * Methode main de l'application
     * @param args tableau d'argument
     * @throws Exception levée durant l'execution
     */
    public static void main(final String[] args) throws Exception {
        ScannerUtil.ouvrir();

        final Commande commandes = new Commande();
        final File file = new File(commandes.getFile().getPath());

        System.out.print(file.getAbsolutePath() + ">");
        String cmd = ScannerUtil.lire();
        Gestion.gestionCommande(file, cmd);

        ScannerUtil.fermer();
    }

}
