/**
 * 
 */
package validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 59013-07-04
 *
 */
public class CommandeValidator {

    /**
     * constructeur par defaut
     */
    public CommandeValidator() {
        super();
    }

    /**
     * permet de valider la commande mypwd
     * 
     * @param cmd lu
     * @return true si le format est respect� sinon false
     */
    public static boolean isValideMyPwd(final String cmd) {
        final String regExpPwd = "^mypwd$";
        final Pattern pattern = Pattern.compile(regExpPwd);
        final Matcher matcher = pattern.matcher(cmd);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    /**
     * permet de valider la commande mydisplay
     * 
     * @param cmd lu
     * @return true si le format est respect� sinon false
     */
    public static boolean isValideMyDisplay(final String cmd) {
        final String regExpPwd = "^mydisplay$";
        final Pattern pattern = Pattern.compile(regExpPwd);
        final Matcher matcher = pattern.matcher(cmd);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    /**
     * permet de valider la commande myrm
     * 
     * @param cmd lu
     * @return true si le format est respect� sinon false
     */
    public static boolean isValideMyRemove(final String cmd) {
        final String regExpRm = "^myrm.*$";
        final Pattern pattern = Pattern.compile(regExpRm);
        final Matcher matcher = pattern.matcher(cmd);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    /**
     * permet de valider la commande myfind
     * 
     * @param cmd lu
     * @return true si le format est respect� sinon false
     */
    public static boolean isValideMyFind(final String cmd) {
        final String regExpFind = "^myfind.*$";
        final Pattern pattern = Pattern.compile(regExpFind);
        final Matcher matcher = pattern.matcher(cmd);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    /**
     * Permet de valider la commande mycp
     * 
     * @param command la commande saisie par l'utilisateur
     * @return true si la command est bonne sinon false
     */
    public static Boolean isValidatorCopier(final String command) {
        final Pattern pattern = Pattern.compile("mycp.*", Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(command.trim());
        if (!matcher.matches()) {
            return false;
        }
        return true;
    }

    /**
     * Permet de valider la commande myls -l
     * 
     * @param command lue
     * @return true si le format de la command est correct sinon false
     */
    public static Boolean isValidatorLister(final String command) {
        final Pattern pattern = Pattern.compile("myls", Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(command.trim());
        if (!matcher.matches()) {
            return false;
        }

        return true;
    }

    /**
     * Permet de valider la commande mymv
     * 
     * @param command lu
     * @return true si le format de la command est correct sinon false
     */
    public static Boolean isValidatorMoveFile(final String command) {
        final Pattern pattern = Pattern.compile("mymv.*", Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(command.trim());
        if (!matcher.matches()) {
            return false;
        }
        return true;
    }

    /**
     * Permet de valider la commande mycd
     * 
     * @param command lu
     * @return true si le format de la command est correct sinon false
     */
    public static Boolean isValidatorlisteRepertoire(final String command) {
        final Pattern pattern = Pattern.compile("mycd.*", Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(command.trim());
        if (!matcher.matches()) {
            return false;
        }
        return true;
    }

    /**
     * Permet de valider la commande mytasklist
     * 
     * @param command lu
     * @return true si le format de la command est correct sinon false
     */
    public static boolean isValidatorMyps(final String command) {
        // TODO encore a faire sur linux
        final Pattern pattern = Pattern.compile("[a-zA-Z0-9\\/.]+", Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(command);
        if (!matcher.matches()) {
            return false;
        }
        return true;
    }

    /**
     * Permet de valider la commande myhelp
     * 
     * @param command lue
     * @return true si le format de la command est correct sinon false
     */
    public static Boolean isValidatorMyhelp(final String command) {
        final Pattern pattern = Pattern.compile("myhelp.*", Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(command.trim());
        if (!matcher.matches()) {
            return false;
        }
        return true;

    }
}
