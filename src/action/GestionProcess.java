package action;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe qui permet de parcourir le fichier /proc <br>
 * de lire des fichiers et construire l'objet Process <br>
 * commande myps executable sur linux
 * 
 * @author Latifa
 *
 */
public class GestionProcess {

    /**
     * statut du process
     */
    static Map<String, String> statusProcess = new HashMap<String, String>();
    /**
     * Liste des pid
     */
    static List<String>        allPid        = new ArrayList<>();

    /**
     * Permet de parcourir le fichier proc<br>
     * et récupére la liste des pid
     */
    public static void getAllPid() {
        final Pattern pattern = Pattern.compile("[1-9]+", Pattern.CASE_INSENSITIVE);
        Matcher matcher;
        final File file = new File("/proc");

        final File[] fichiers = file.listFiles();
        try {
            for (final File file2 : fichiers) {
                matcher = pattern.matcher(file2.getName());
                while (matcher.find()) {
                    allPid.add(file2.getName());
                }
            }

        } catch (final Exception e) {
            System.err.println(e.getMessage());
        }

    }

    /**
     * Permie de boucler sur la liste<br>
     * des pid pour recupérer leur status et mapper avec l'objet <br>
     * process
     */
    public static void allStatusAllpid() {
        for (final String pid : allPid) {
            getStatusAllPid(pid);
            remplirProcess();
        }
        allPid.clear();
    }

    /**
     * La methode recupere les status<br>
     * Pour un process
     * 
     * @param pid numéro process
     */
    public static void getStatusAllPid(final String pid) {

        final File file = new File("/proc/" + pid + "/status");
        final Pattern pattern = Pattern.compile("[^:]+", Pattern.CASE_INSENSITIVE);
        Matcher matcher;
        final List<String> listes = new ArrayList<>();
        
        try (final FileInputStream fis = new FileInputStream(file);
             final InputStreamReader isr = new InputStreamReader(fis);
             final BufferedReader br = new BufferedReader(isr)) {

            String line;

            while ((line = br.readLine()) != null) {
                matcher = pattern.matcher(line);
                while (matcher.find()) {
                    listes.add(matcher.group());
                }
            }
        } catch (final Exception err) {
            err.printStackTrace();
        }

        if (listes.size() % 2 == 0) {
            for (int j = 0; j < listes.size(); j = j + 2) {
                statusProcess.put(listes.get(j).trim(), listes.get(j + 1).trim());

            }
        } else {
            for (int j = 0; j < listes.size() - 1; j = j + 2) {
                statusProcess.put(listes.get(j).trim(), listes.get(j + 1).trim());

            }
        }

    }

    /**
     * Permet de remplir l'objet process <br>
     * et d'ajouter à la liste des processus
     * 
     */
    public static void remplirProcess() {
        Process process = new Process();
        for (final Entry<String, String> entry : statusProcess.entrySet()) {
            if (entry.getKey().equalsIgnoreCase("name")) {
                process.setName(entry.getValue());
            }
            if (entry.getKey().equalsIgnoreCase("Cpus_allowed_list")) {
                process.setCpu(entry.getValue());
            }
            if (entry.getKey().equalsIgnoreCase("pid")) {
                process.setPid(entry.getValue());
            }
            if (entry.getKey().equalsIgnoreCase("state")) {
                process.setStat(entry.getValue());
            }
            if (entry.getKey().equalsIgnoreCase("Mems_allowed_list")) {
                process.setMem(entry.getValue());
            }
            if (entry.getKey().equalsIgnoreCase("RssFile")) {
                process.setRss(entry.getValue());
            }
        }
        statusProcess.clear();
        Process.listProcess.add(process);
    }

}
