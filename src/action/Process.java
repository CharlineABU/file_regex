package action;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe de l'Objet process
 * 
 * @author Latifa
 *
 */
public class Process {
    /**
     * nom du process
     */
    private String       name;
    /**
     * pid du process
     */
    private String       pid;
    /**
     * cpu du process
     */
    private String       cpu;
    /**
     * mem du process
     */
    private String       mem;
    /**
     * rss du process
     */
    private String       rss;
    /**
     * stat du process
     */
    private String       stat;

    /**
     * liste des process
     */
    static List<Process> listProcess = new ArrayList<>();

    /**
     * Constructeur par defaut
     */
    public Process() {
        super();
    }

    /**
     * Constructeur avec field : permet d'instancier un process avec ses field
     * 
     * @param pid du process
     * @param cpu du process
     * @param mem du process
     * @param rss du process
     * @param stats du process
     * @param name du process
     */
    public Process(final String pid, final String cpu, final String mem, final String rss, final String stats, final String name) {
        super();
        this.name = name;
        this.pid = pid;
        this.stat = stats;
        this.cpu = cpu;
        this.mem = mem;
        this.rss = rss;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.pid + "\t\t" + this.cpu + "\t\t" + this.mem + "\t\t" + this.rss + "\t\t" + this.stat + "\t\t\t" + this.name + "";
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the pid
     */
    public String getPid() {
        return pid;
    }

    /**
     * @param pid the pid to set
     */
    public void setPid(final String pid) {
        this.pid = pid;
    }

    /**
     * @return the cpu
     */
    public String getCpu() {
        return cpu;
    }

    /**
     * @param cpu the cpu to set
     */
    public void setCpu(final String cpu) {
        this.cpu = cpu;
    }

    /**
     * @return the mem
     */
    public String getMem() {
        return mem;
    }

    /**
     * @param mem the mem to set
     */
    public void setMem(final String mem) {
        this.mem = mem;
    }

    /**
     * @return the stat
     */
    public String getStat() {
        return stat;
    }

    /**
     * @param stat the stat to set
     */
    public void setStat(final String stat) {
        this.stat = stat;
    }

    /**
     * @return the rss
     */
    public String getRss() {
        return rss;
    }

    /**
     * @param rss the rss to set
     */
    public void setRss(final String rss) {
        this.rss = rss;
    }

    /**
     * @return the listProcess
     */
    public static List<Process> getListProcess() {
        return listProcess;
    }

    /**
     * @param listProcess the listProcess to set
     */
    public static void setListProcess(final List<Process> listProcess) {
        Process.listProcess = listProcess;
    }

}
