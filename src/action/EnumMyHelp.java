package action;

/**
 * Enum des commandes
 * 
 * @author Latifa
 *
 */
public enum EnumMyHelp {

    MYCP("copie un fichier vers un autre fichier"), MYMV("Déplace un fichier d'un répertoire à  un autre"), MYLS("Affiche la liste des fichiers et des sous-répertoires d'un répertoire"), MYCD(
                    "Changer de répertoire"), MYDISPLAY("Affiche le contenu d'un fichier à  consulter"), MYRM("Supprime un fichier"), MYFIND(
                                    "Recherche un fichier"), MYHELP("Affiche des informations sur les commandes de Windows"), MYPWD(
                                                    "Affiche le chemin d'accès vers le répertoire ou se situe l'utilisatuer"), MYPS("Affiche gestion de taches");

    /**
     * description de l'enum
     */
    private String description;

    /**
     * Constructeur par defaut
     * 
     * @param description de l'enum
     */
    private EnumMyHelp(final String description) {
        this.description = description;
    }

    /**
     * @return description
     */
    public String getDescription() {
        return description;
    }

}
