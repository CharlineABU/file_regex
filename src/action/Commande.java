/**
 * 
 */
package action;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui implemente les commandes
 * 
 * @author 59013-07-04
 *
 */
public class Commande {
    /**
     * Instance de file
     */
    private File file = new File("C:\\");

    /**
     * Permet d'afficher le chemin de l'emplacement du fichier <br>
     * commande mypwd
     * 
     * @param file de l'emplacement en cours
     * @return le path du fichier sinon message d'erreur
     */
    public static String pwd(final File file) {
        String cheminAcces = "";
        try {
            cheminAcces = file.getAbsolutePath();
            System.out.println("\t"+cheminAcces+"\n");
            return cheminAcces;
        } catch (final SecurityException securityException) {
            System.err.println("Accès non autorisé !");
            return "L'emplacement n'est pas accesible !";
        }
    }

    /**
     * Permet d'afficher le contenu du fichier <br>
     * commande mydisplay
     * 
     * @param file de l'emplacement en cours
     * @param chaine fichier à consulter
     */
    public static void displayFichier(final File file, final String chaine) {

        final File fichier = new File(file.getPath() + "\\" + chaine);

        if (fichier.isFile() && fichier.exists()) {
            try {
                final String path = pwd(fichier);
                final InputStream flux = new FileInputStream(path);
                final InputStreamReader lecture = new InputStreamReader(flux);
                final BufferedReader buff = new BufferedReader(lecture);
                String ligne;

                while ((ligne = buff.readLine()) != null) {
                    System.out.println(ligne);
                }
                buff.close();

            } catch (final FileNotFoundException fileNotFoundException) {
                System.out.println("Le fichier n'existe pas !");
            } catch (final SecurityException securityException) {
                System.out.println("Accès refussé !");
            } catch (final IOException ioException) {
                System.out.println("Execution interompue !");
            }
        } else if (fichier.isDirectory()) {
            System.out.println("C'est un repertoire !");
        } else if (!fichier.exists()) {
            System.out.println("Le fichier n'existe pas !");
        }
    }

    /**
     * Permet de supprimer un fichier ou un repertoire <br>
     * commande myrm
     * 
     * @param f path de l'emplacement en cours
     * @param chaine path du fichier à supprimer
     * @return true si suppression OK sinon false
     */
    public static boolean removeFichier(final File f, final String chaine) {

        try {
            final List<File> listeDesFiles = findFileByName(f, chaine);
            int i = 1;

            for (final File file : listeDesFiles) {
                System.out.println("\t" + i + " - " + file.getPath());
                i++;
            }

            System.out.print("\n\tIndiquez le numero du fichier : ");
            int choix = Integer.valueOf(ScannerUtil.lire());
            while (choix < 1 | choix >= i) {
                System.out.print("\tFaite un choix dans la liste : ");
                choix = Integer.valueOf(ScannerUtil.lire());
            }

            System.out.print("\tConfirmer la suppression (O)ui ou (N)on :");
            String rep = ScannerUtil.lire();
            while (!rep.equals("O") && !rep.equals("N")) {
                System.out.print("\tConfirmer avec (O)ui ou (N)on : ");
                rep = ScannerUtil.lire();
            }

            if (rep.equals("O") && listeDesFiles.get(choix - 1).delete() == true) {
                System.out.println("\tDELETE OK !");
                return true;
            } else if (rep.equals("N")) {
                System.out.println("\tANNULEE !");
            }

        } catch (final SecurityException securityException) {
            System.out.println("Accès refusé !");
        } catch (final Exception e) {
            System.out.println("Saisie Non conforme !");
        }
        return false;
    }

    /**
     * Affiche tous les fichiers trouvés <br>
     * commande myfind
     * 
     * @param path de l'emplacement en cours
     * @param chaine du fichier à chercher
     */
    public static void myFind(final File path, final String chaine) {
        try {
            for (final File f : findFileByName(path, chaine)) {
                System.out.println("\t" + f.getAbsoluteFile());
            }
        } catch (final SecurityException securityException) {
            System.out.println("Accès refusé !");
        }
    }

    /**
     * Permet de chercher tous les fichiers qui ont le même nom
     * 
     * @param f file de l'emplacement en cours
     * @param chaine fichier à chercher
     * @return resulFiles la liste de tous les fichiers trouvées
     */
    public static List<File> findFileByName(final File f, final String chaine) {

        final List<File> resulFiles = new ArrayList<File>();

        try {
            if (!f.isDirectory() && f.getName().equalsIgnoreCase(chaine)) {
                resulFiles.add(f);
                return resulFiles;
            } else if (f.exists() && f.isDirectory()) {

                final File[] fichiers = f.listFiles();
                for (final File file : fichiers) {
                    if (file.isDirectory() && f.getName().equalsIgnoreCase(chaine)) {
                        resulFiles.add(file);
                    }
                    resulFiles.addAll(findFileByName(file, chaine));
                }
            }
        } catch (final SecurityException securityException) {
            System.out.println("Accès refusé !");
        } catch (final Exception e) {
            System.out.println("Non conforme !");
        }

        return resulFiles;
    }

    /**
     * Permet d'afficher la liste de toutes les commandes <br>
     * commande myhelp
     * 
     */
    public static void myhelp() {
        System.out.println("\t" + EnumMyHelp.MYCD + "\t\t\t: " + EnumMyHelp.MYCD.getDescription());
        System.out.println("\t" + EnumMyHelp.MYCP + "\t\t\t: " + EnumMyHelp.MYCP.getDescription());
        System.out.println("\t" + EnumMyHelp.MYDISPLAY + "\t\t: " + EnumMyHelp.MYDISPLAY.getDescription());
        System.out.println("\t" + EnumMyHelp.MYFIND + "\t\t\t: " + EnumMyHelp.MYFIND.getDescription());
        System.out.println("\t" + EnumMyHelp.MYHELP + "\t\t\t: " + EnumMyHelp.MYHELP.getDescription());
        System.out.println("\t" + EnumMyHelp.MYLS + "\t\t\t: " + EnumMyHelp.MYLS.getDescription());
        System.out.println("\t" + EnumMyHelp.MYMV + "\t\t\t: " + EnumMyHelp.MYMV.getDescription());
        System.out.println("\t" + EnumMyHelp.MYPS + "\t\t\t: " + EnumMyHelp.MYPS.getDescription());
        System.out.println("\t" + EnumMyHelp.MYPWD + "\t\t\t: " + EnumMyHelp.MYPWD.getDescription());
        System.out.println("\t" + EnumMyHelp.MYRM + "\t\t\t: " + EnumMyHelp.MYRM.getDescription());

    }

    /**
     * Permet de copier un fichier a une destination donnee <br>
     * commande cp
     * 
     * @param sourcePath du fichier à copier
     * @param destinationPath du fichier à copier
     * @param file de l'emplacement en cours
     * @return true si copie OK sinon false
     */
    public static boolean copierFichier(final File file, final String sourcePath, final String destinationPath) {

        final File source = new File(file.getAbsolutePath() + "\\" + sourcePath);
        final File destination = new File(file.getAbsolutePath() + "\\" + destinationPath);

        if (source.exists() && destination.exists()) {
            try (final FileChannel in = new FileInputStream(source).getChannel();
                 final FileChannel out = new FileOutputStream(destination).getChannel()) {
                in.transferTo(0, in.size(), out);
                return true;
            } catch (final IOException e) {
                System.out.println(e.getMessage());
                return false;
            }
        }
        System.out.println("Le fichier spécifié est introuvable.");
        return false;
    }

    /**
     * Permet de lister le contenue d'un repertoire <br>
     * commande myls
     * 
     * @param file path de l'emplacement en cours
     */
    public static void listerFichier(final File file) {

        final File[] fichiers = file.listFiles();

        if (file.exists() && file.isDirectory()) {
            try {
                for (int i = 0; i < fichiers.length; i++) {
                    System.out.println("	" + fichiers[i].getName());
                }
            } catch (final Exception e) {
                System.err.println(e.getMessage());
            }

        } else {
            System.err.println(file + " : Erreur de lecture.");
        }

    }

    /**
     * Permet de changer de repertoire s'il existe <br>
     * commande mycd
     * 
     * @param file path d'origine
     * @param str path de destination
     */
    public void changeRepertoire(final File file, final String str) {

        if (file.exists() && file != null) {
            if ("..".equalsIgnoreCase(str)) {
                this.file = file.getParentFile();
            }
            if (!"..".equalsIgnoreCase(str)) {
                String str1 = file.getAbsoluteFile().toString();
                File f = new File(str1 + "\\" + str);
                if (f.isDirectory()) {
                    this.file = f;
                }
            }
        } else {
            System.err.println(file + " : Erreur de lecture.");
        }
    }

    /**
     * Permet de remonte dans le répertoire parent si existe <br>
     * 
     * @param file path d'origine
     */
    public void changeRepertoireRemonte(final File file) {
        //TODO try catch pour exist = SecurityException 
        if (file.exists()) {
            this.file = file.getParentFile();
        } else {
            System.err.println(file + " : Erreur de lecture.");
        }
    }

    /**
     * Permet de deplacer un repertoire à une destianation donnee <br>
     * commande mymv
     * 
     * @param file emplacement en cours
     * @param fileSource emplacement d'origine
     * @param filedestination emplacement de destination
     */
    public static void moveFile(final File file, final String fileSource, final String filedestination) {
        //TODO ajouter catch Exception pour la methode move
        final Path source = Paths.get(file.getAbsolutePath() + "\\" + fileSource);
        final Path destination = Paths.get(file.getAbsolutePath() + "\\" + filedestination + "\\" + fileSource);

        if (source.isAbsolute() && destination.isAbsolute()) {
            try {
                Files.move(source, destination, StandardCopyOption.REPLACE_EXISTING);
            } catch (final IOException e) {
                System.err.println(e.getMessage());
            }
        } else {
            System.out.println("Le chemin d'accès spécifié est introuvable.");
        }
    }

    /**
     * @return file de l'emplacement
     */
    public File getFile() {
        return file;
    }

    /**
     * @param file de l'emplacement
     */
    public void setFile(final File file) {
        this.file = file;
    }

}
