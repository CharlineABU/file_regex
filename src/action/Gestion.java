/**
 * 
 */
package action;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import validator.CommandeValidator;

/**
 * Classe qui gère le choix des commandes et la validation de leur format
 * 
 * @author Charline
 *
 */
public class Gestion {

    /**
     * Constructeur par defaut
     */
    public Gestion() {
        super();
    }

    /**
     * permet de gerer les commandes lues
     * 
     * @param file de l'emplacement en cours
     * @param commande lue
     */
    public static void gestionCommande(final File file, final String commande) {

        File path = new File(file.getPath());
        final Commande methodesCmd = new Commande();
        String[] cmdTab = extractionCommande(commande);
        String chaine = extractionsMoveFilePourMycdMyfind(commande);
        String cmd = cmdTab[0];
        String saisie = "";
        while (cmd != "exit") {

            if (gestionValidator(cmd.toLowerCase())) {

                String source = cmdTab[1];
                String destination = cmdTab[2];

                switch ((cmd.toLowerCase())) {
                    case "mypwd" :
                        Commande.pwd(path);
                        break;

                    case "mydisplay" :
                        Commande.displayFichier(path, source);
                        break;

                    case "myrm" :
                        Commande.removeFichier(path, source);

                        break;
                    case "myfind" :
                        Commande.myFind(path, source);
                        break;

                    case "myhelp" :
                        Commande.myhelp();
                        break;

                    case "mycd" :
                        methodesCmd.changeRepertoire(path, chaine);
                        break;

                    case "mymv" :
                        Commande.moveFile(path, source, destination);
                        break;

                    case "mycp" :
                        Commande.copierFichier(path, source, destination);
                        break;

                    case "myls" :
                        Commande.listerFichier(path);
                        break;
                    case "myps" :
                        //Commande.myps;
                        //TODO faire methode myps dans Commande
                        GestionProcess.getAllPid();
                        GestionProcess.allStatusAllpid();
                        System.out.println();
                        System.out.println("PID\t\t%CPU\t\t%MEM\t\tRSS\t\tSTAT\t\t\t\tNAME");
                        for (final Process process : Process.listProcess) {
                            System.out.println(process.toString());
                        }

                        break;
                    case "mycd.." :
                        methodesCmd.changeRepertoireRemonte(path);
                        break;
                    case "exit" :
                        System.exit(0);
                        break;

                    default :
                        System.out.print(path.getAbsolutePath() + "> commande inconnu !");
                        break;
                }
            } else {
                System.out.println(path.getAbsolutePath() + "> commande inconnu !");
            }
            path = new File(methodesCmd.getFile().getAbsolutePath());
            System.out.print(path.getAbsolutePath() + ">");
            saisie = ScannerUtil.lire();
            cmdTab = extractionCommande(saisie);
            cmd = cmdTab[0];
            chaine = extractionsMoveFilePourMycdMyfind(saisie);
        }
        System.exit(0);
    }

    /**
     * Permet de gerer les validators de commande
     * 
     * @param commande lue
     * @return true si le format est correct sinon false
     */
    public static boolean gestionValidator(final String commande) {
        Boolean valide = false;
        switch (commande) {
            case "mypwd" :
                valide = CommandeValidator.isValideMyPwd(commande);
                break;
            case "mydisplay" :
                valide = CommandeValidator.isValideMyDisplay(commande);
                break;
            case "myrm" :
                valide = CommandeValidator.isValideMyRemove(commande);
                break;
            case "myfind" :
                valide = CommandeValidator.isValideMyFind(commande);
                break;
            case "mycd" :
                valide = CommandeValidator.isValidatorlisteRepertoire(commande);
                break;
            case "mycd.." :
                valide = CommandeValidator.isValidatorlisteRepertoire(commande);
                break;
            case "mycp" :
                valide = CommandeValidator.isValidatorCopier(commande);
                break;
            case "mymv" :
                valide = CommandeValidator.isValidatorMoveFile(commande);
                break;
            case "myls" :
                valide = CommandeValidator.isValidatorLister(commande);
                break;
            case "myhelp" :
                valide = CommandeValidator.isValidatorMyhelp(commande);
                break;
            case "myps" :
                valide = CommandeValidator.isValidatorMyps(commande);
                break;
            case "exit" :
                valide = true;
                break;
            default :
                valide = false;
                break;
        }

        return valide;
    }

    /**
     * Permet de décomposer la commande lue
     * 
     * @param commande lue
     * @return tab tableau contenant les composants de la commande
     */
    public static String[] extractionCommande(final String commande) {
        final String[] tab = new String[20];
        int i = 0;
        final Pattern pattern = Pattern.compile("[a-zA-Z0-9_\\/.-]+", Pattern.CASE_INSENSITIVE);
        final Matcher matches = pattern.matcher(commande);

        while (matches.find()) {
            tab[i] = matches.group();
            i++;
        }
        return tab;
    }

    /**
     * Permet de recuperer le reste de la chaine de la commande lue
     * 
     * @param commande lue
     * @return chaine reste de la chaine de la commande lue
     */
    public static String extractionsMoveFilePourMycdMyfind(final String commande) {
        int i = 0;
        String chaine = " ";
        final Pattern pattern = Pattern.compile(" [a-zA-Z0-9_ \\\\/.-]+", Pattern.CASE_INSENSITIVE);
        final Matcher matches = pattern.matcher(commande.trim());

        while (matches.find()) {
            chaine = matches.group(0);
            i++;
        }
        return chaine.trim();
    }

}
